/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author GioXsanX
 */
public class Person {
    private String id;
    private String firstName;
    private String secondName;
    private String lastName1;
    private String lastName2;
    private Date birthDate;
    private String age;
    private String departament;
    private String Municipio;
    private String genero;

    public Person() {
    }

    public Person(String id, String firstName, String secondName, String lastName1, String lastName2, Date birthDate, String age, String departament, String Municipio, String genero) {
        this.id = id;
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName1 = lastName1;
        this.lastName2 = lastName2;
        this.birthDate = birthDate;
        this.age = age;
        this.departament = departament;
        this.Municipio = Municipio;
        this.genero = genero;
    }

    public String getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setLastName1(String lastName1) {
        this.lastName1 = lastName1;
    }

    public void setLastName2(String lastName2) {
        this.lastName2 = lastName2;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setDepartament(String departament) {
        this.departament = departament;
    }

    public void setMunicipio(String Municipio) {
        this.Municipio = Municipio;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getLastName1() {
        return lastName1;
    }

    public String getLastName2() {
        return lastName2;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public String getAge() {
        return age;
    }

    public String getDepartament() {
        return departament;
    }

    public String getMunicipio() {
        return Municipio;
    }

    public String getGenero() {
        return genero;
    }
}
    
    

  
